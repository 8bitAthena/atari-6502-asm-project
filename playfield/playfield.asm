	
        processor 6502
        
	include "vcs.h"
	include "macro.h"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Start ROM code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        seg
	org  $f000

Reset:
	CLEAN_START
        
        ldx #$80	; blue bg colour
        stx COLUBK
        
        lda #$1C	; yellow playfield 
        sta COLUPF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Start new frame by config vblank and vsync
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
StartFrame:
	lda #02
        sta VBLANK	; turn on vblank
        sta VSYNC	; turn on vsync
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Gen 3 lines of VSYNC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	REPEAT 3	;dasm assembler allows this
        	sta WSYNC
        REPEND
        
        lda #0
        sta VSYNC	; turn off VSYNC
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Let TIA Output 37 Recommended lines of VBLANK
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	REPEAT 37
        	sta WSYNC
        REPEND
        
        lda #0
        sta VBLANK	; turn off vblank

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set CTRLPF reg to allow playfield reflection (the reg that tells tia
;; if want to repeat or reflect pf)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ldx #%00000001	; load the D0 bit as 1 => reflect in the CTRLPF reg
	stx CTRLPF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Draw 192 visible scanlines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; 1. first seven scan lines must render no pf, only bg
        ldx #0
        stx PF0		; first third of playfield (rep with 4 bits)
        stx PF1		; second third of playfield
        stx PF2		; disable whole play field and repeat for the seven
        		; scanlines
       	REPEAT 7
            sta WSYNC	; in repeat, must wait for seven scanlines
                	; to be rendered
        REPEND
        
        ; 2. Render next 7 scanlines that are reflected
        ldx #%11100000	; set PF0 to 1110 (least significant bit is displayed first
        stx PF0         ; therefore the 0 will rep blue on the lhs of the playfield,
                        ; 1 will rep white). 0's after four bits ignored
        ldx #%11111111	; store just white in PF1 and PF2 to be rendered for the
        stx PF1
        stx PF2
        
        REPEAT 7
            sta WSYNC
        REPEND
        
        ; 3. Set next 164 scanlines where just pf0 has third bit white
        ldx #%00100000	
        stx PF0
        
        ldx #0
        stx PF1
        stx PF2
        
        REPEAT 164
	    sta WSYNC
        REPEND 
        
        ; 4. repeat top two steps in revers order to gen bottom of playfield
        ldx #%11100000	; set PF0 to 1110 (least significant bit is displayed first
        stx PF0         ; therefore the 0 will rep blue on the lhs of the playfield,
                        ; 1 will rep white). 0's after four bits ignored
        ldx #%11111111	; store just white in PF1 and PF2 to be rendered for the
        stx PF1
        stx PF2
        
        REPEAT 7
            sta WSYNC
        REPEND
        
        ldx #0
        stx PF0		; first third of playfield (rep with 4 bits)
        stx PF1		; second third of playfield
        stx PF2		; disable whole play field and repeat for the seven
        		; scanlines
       	REPEAT 7
            sta WSYNC	; in repeat, must wait for seven scanlines
                	; to be rendered
        REPEND
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Output 30 VBLANK overscan lines to complete frame
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
	lda #2		; turn on VBLANK
        sta VBLANK
        
        REPEAT 30
            sta WSYNC	; wait for scanlines to be rendered  
        REPEND
        
        lda #0		; turn off vblank
        sta VBLANK
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Loop to next frame
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	jmp StartFrame
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Complete ROM size
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        org $fffc
        .word Reset
        .word Reset
	
