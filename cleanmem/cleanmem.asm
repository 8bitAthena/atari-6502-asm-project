	processor 6502

	seg code
	org $F000 		; Start of ROM cartridge, define code origin

Start:
	sei			; Disable interrupts
	cld			; Disable BCD decimal math mode
	ldx #$FF		; Loads the X register with the #$FF (Start setting the SP)
	txs			; Transfer the X register to the Stack pointer

; Clear the page zero region ie. entire ram and also entire TIA register reg $00 to $FF

	lda #0			; A = 0
	ldx #$FF		; X= #$FF the decimal of the hex value of the reg address (loop ptr)
	sta $FF			; make sure $FF zeroed before loop start
MemLoop:
	dex			; X--
	sta $0,X		; store value of A reg, in the mem addr (0 + whatever's in X reg)
	bne MemLoop		; branch if != 0 back to MemLoop (until z-flag set)
				; sta doesn't set any flags, therefore won't mess up the bne from dec

; Fill ROM size to 4kb to close off Atari cartridge

	org $FFFC		; jump to FFFC as atari expects to find two bytes at this position
	.word Start		; word is two bytes, expects to find reset address (where prog start)
	.word Start		; second byte at Start. This is req for cartridge to run. 
				; Interrupt vector at $FFFE (unused in VCS)
